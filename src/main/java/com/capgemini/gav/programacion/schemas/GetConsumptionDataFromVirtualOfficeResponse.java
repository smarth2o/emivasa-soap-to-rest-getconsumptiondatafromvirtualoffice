
package com.capgemini.gav.programacion.schemas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetConsumptionDataFromVirtualOffice" type="{http://www.capgemini.com/gav/programacion/schemas}GetConsumptionDataFromVirtualOfficeAnswerType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getConsumptionDataFromVirtualOffice"
})
@XmlRootElement(name = "GetConsumptionDataFromVirtualOfficeResponse")
public class GetConsumptionDataFromVirtualOfficeResponse {

    @XmlElement(name = "GetConsumptionDataFromVirtualOffice", required = true)
    protected GetConsumptionDataFromVirtualOfficeAnswerType getConsumptionDataFromVirtualOffice;

    /**
     * Gets the value of the getConsumptionDataFromVirtualOffice property.
     * 
     * @return
     *     possible object is
     *     {@link GetConsumptionDataFromVirtualOfficeAnswerType }
     *     
     */
    public GetConsumptionDataFromVirtualOfficeAnswerType getGetConsumptionDataFromVirtualOffice() {
        return getConsumptionDataFromVirtualOffice;
    }

    /**
     * Sets the value of the getConsumptionDataFromVirtualOffice property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetConsumptionDataFromVirtualOfficeAnswerType }
     *     
     */
    public void setGetConsumptionDataFromVirtualOffice(GetConsumptionDataFromVirtualOfficeAnswerType value) {
        this.getConsumptionDataFromVirtualOffice = value;
    }

}
