
package com.capgemini.gav.programacion.schemas;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.capgemini.gav.programacion.schemas package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.capgemini.gav.programacion.schemas
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetConsumptionDataFromVirtualOfficeRequest }
     * 
     */
    public GetConsumptionDataFromVirtualOfficeRequest createGetConsumptionDataFromVirtualOfficeRequest() {
        return new GetConsumptionDataFromVirtualOfficeRequest();
    }

    /**
     * Create an instance of {@link GetConsumptionDataFromVirtualOfficeType }
     * 
     */
    public GetConsumptionDataFromVirtualOfficeType createGetConsumptionDataFromVirtualOfficeType() {
        return new GetConsumptionDataFromVirtualOfficeType();
    }

    /**
     * Create an instance of {@link GetConsumptionDataFromVirtualOfficeResponse }
     * 
     */
    public GetConsumptionDataFromVirtualOfficeResponse createGetConsumptionDataFromVirtualOfficeResponse() {
        return new GetConsumptionDataFromVirtualOfficeResponse();
    }

    /**
     * Create an instance of {@link GetConsumptionDataFromVirtualOfficeAnswerType }
     * 
     */
    public GetConsumptionDataFromVirtualOfficeAnswerType createGetConsumptionDataFromVirtualOfficeAnswerType() {
        return new GetConsumptionDataFromVirtualOfficeAnswerType();
    }

    /**
     * Create an instance of {@link MeterReadingsType }
     * 
     */
    public MeterReadingsType createMeterReadingsType() {
        return new MeterReadingsType();
    }

}
