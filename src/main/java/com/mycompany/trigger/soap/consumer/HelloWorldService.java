/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.trigger.soap.consumer;

import com.capgemini.gav.programacion.schemas.GetConsumptionDataFromVirtualOfficeRequest;
import com.capgemini.gav.programacion.schemas.GetConsumptionDataFromVirtualOfficeResponse;
import com.capgemini.gav.programacion.schemas.GetConsumptionDataFromVirtualOfficeType;
import com.capgemini.gav.programacion.schemas.MeterReadingsType;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author ThinkPad
 */
public class HelloWorldService implements IHelloWorldService {

    static Logger log = Logger.getLogger(HelloWorldService.class);

    public HelloWorldService() {
        log.info(HelloWorldService.class.getName() + " is up and running");
    }

    @Override
    public String sayHello(String user_id,String dataStart,String dataStop) {

        log.info("Entering DataProvioining WS");
        GetConsumptionDataFromVirtualOfficeRequest request;
        GetConsumptionDataFromVirtualOfficeType type = new GetConsumptionDataFromVirtualOfficeType();
        type.setDataStart(dataStart);
        type.setDataStop(dataStop);
        type.setUserId(user_id);
        request = new GetConsumptionDataFromVirtualOfficeRequest();
        request.setGetConsumptionDataFromVirtualOffice(type);
        GetConsumptionDataFromVirtualOfficeResponse response = getConsumptionDataFromVirtualOffice(request);
        String userId = response.getGetConsumptionDataFromVirtualOffice().getUserId();
        String zipcode = response.getGetConsumptionDataFromVirtualOffice().getZipcode();
        String email = response.getGetConsumptionDataFromVirtualOffice().getEmail();
        List<MeterReadingsType> list = response.getGetConsumptionDataFromVirtualOffice().getMeterReadings();
        
        ObjectMapper mapper = new ObjectMapper(); 
        Map<String,Object> json=new HashMap<String, Object>();
        json.put("user_id", userId);
        json.put("zipcode", zipcode);
        json.put("email", email);
        
        List<Object> meterReadings=new ArrayList<Object>();
        json.put("meter_readings", meterReadings);
        //log.info("user_id " + userId);
        
        for (MeterReadingsType m : list) {
            Map<String, Object> entry=new HashMap<String, Object>();                    
            //log.info("Reading " + m.getSmartmeterId() + " " + m.getTimestamp() + " " + m.getValue());
            entry.put("smartmeter_id", m.getSmartmeterId());
            entry.put("timestamp", m.getTimestamp());
            entry.put("value", m.getValue());
            meterReadings.add(entry);
        }
        
        String result="{}";
        try {
            result = mapper.writeValueAsString(json);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(HelloWorldService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return result;
    }

    private static GetConsumptionDataFromVirtualOfficeResponse getConsumptionDataFromVirtualOffice(com.capgemini.gav.programacion.schemas.GetConsumptionDataFromVirtualOfficeRequest getConsumptionDataFromVirtualOfficeRequest) {
        com.capgemini.gav.programacion.schemas.GetConsumptionDataFromVirtualOfficeService service = new com.capgemini.gav.programacion.schemas.GetConsumptionDataFromVirtualOfficeService();
        com.capgemini.gav.programacion.schemas.GetConsumptionDataFromVirtualOfficePort port = service.getGetConsumptionDataFromVirtualOfficePortSoap11();
        return port.getConsumptionDataFromVirtualOffice(getConsumptionDataFromVirtualOfficeRequest);
    }
}
