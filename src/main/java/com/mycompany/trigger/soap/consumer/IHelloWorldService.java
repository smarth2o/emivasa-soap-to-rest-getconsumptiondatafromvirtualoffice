/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.trigger.soap.consumer;

import javax.jws.WebService;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author ThinkPad
 */
@WebService
@Produces(MediaType.TEXT_PLAIN)
@Path("/DataProvisioningExample")
public interface IHelloWorldService {

    @GET
    @Path("/dataProvisioningExample")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON,MediaType.TEXT_PLAIN})
    String sayHello(@QueryParam("user_id") String user_id,@QueryParam("data_start") String dataStart,@QueryParam("data_stop") String dataStop);
}
